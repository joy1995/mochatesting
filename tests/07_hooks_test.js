const formatter = '        ';
var should = require('should');
dataDriven = require('data-driven');
assert = require('assert');
var testData = [{country: 'NIL'}];

describe('7. Hooks', function(){


  describe('#simply hooks', function(){
    before(() => console.log(formatter + 'before' + '\n'));
    after(() => console.log(formatter + 'after\n'));
    beforeEach(() => console.log(formatter + 'beforeEach'));
    afterEach(() => console.log(formatter + 'afterEach' + '\n'));

    it('test 1', (done) => setImmediate(done));
    it('test 2', (done) => setImmediate(done));
  });



  describe('#level hooks', function() {
		var sharedData = 'dummy data';

		beforeEach(function() {
      console.log('\n' + formatter + 'beforeEach');
			this.sharedData = sharedData;
		});

		dataDriven([{}], function() {
      this.syncData = 'not dummy data'
      this.asyncData = 'not dummy data'

			before(function(ctx) {
        console.log(formatter + 'before')
				this.syncData = sharedData;
			});

			it('should pass appropriate this object to sync test function', function(ctx) {
				this.sharedData.should.equal(sharedData);
				this.syncData.should.equal(sharedData);
        console.log(formatter + 'sync');
			});

      before(function(ctx, done) {
        console.log(formatter + 'before2');
				this.asyncData = sharedData;
				done();
			});

			it('should pass appropriate this object to async test function', function(ctx, done) {
        setImmediate(() => {
          this.sharedData.should.equal(sharedData);
  				this.asyncData.should.equal(sharedData);
          console.log(formatter + 'async');
  				done();
        });

			});

		});
	});
});
