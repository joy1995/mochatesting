const assert = require('assert');

describe('4. Promise', function() {
  describe('#return new promise', function() {
      it('should complete this test', function (done) {
        return new Promise(function (resolve) {
          assert.ok(true);
          resolve();
          done();
        });
      });
  });

  describe('#specify new callback', function() {
    it('should complete this test', function (done) {
        new Promise(function (resolve) {
          assert.ok(true);
          resolve();
        })
        .then(done);
    });
  });

  describe('#overspecified method', function() {
    it('should not complete this test', function (done) {
      return new Promise(function (resolve) {
        assert.ok(true);
        resolve();
      })
      .then(done);
    });
  });

});
