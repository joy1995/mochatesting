var data_driven = require('data-driven');
var assert = require('assert');


class MyExample {
  constructor(name) {
    this.name = name;
  }

  some_async_call(done) {
    setTimeout(done, 100);
  }

  some_async_call2(data, callback) {
    setImmediate(callback(data*10));
  }
}

describe('3. Async Programming', function() {
  var myExample = new MyExample('MyFirstExample');

  describe('#wait the finish', function() {
    it('should compute without error', function(done) {
      myExample.some_async_call(done);
    });
  });

  describe('#data driven', function() {
    data_driven([{name: 'negative', data: -1, expected: -10},{name: 'positive', data: 1, expected: 10}], function() {
        it('should do something ascynchronous with {name} numbers', function(ctx,done) {
            myExample.some_async_call2(ctx.data, function(result) {
                assert.equal(ctx.expected, result)
                done()
            })
        })
    })
  });
});
