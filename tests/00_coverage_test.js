const assert = require('assert');

describe('0. Coverage', function() {
  describe('#istanbul reporter', function() {
    guard = false;
    count = 0;
    it('first one is covered', function(done) {
      count++;
      done();
    });

    if(guard) {
      it('second one isn\'t covered', function(done) {
        count++;
        done();
      });
    }

    it('only one is covered', function(done) {
      assert.equal(count, 1);
      done();
    });
  });
});
