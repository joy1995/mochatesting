const assert = require('assert');
var data_driven = require('data-driven');

describe('1. Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
  describe('#indexOf2()', function() {
    it('should not return -1 when the value is present', function() {
      assert.notEqual([1,2,3,4,5].indexOf(4), -1);
    });
  });
  describe('#data_driven', function(){
      data_driven([{value: 1, expected: 0},{value: 5, expected: -1},{value: 2, expected: 1}], function() {
          it('should return -1 when the value is not present when searching for {value}', function(ctx){
              assert.equal([1,2,3].indexOf(ctx.value), ctx.expected);
          })
      })
  })
  describe('#indexOf()', function() {
    // pending test below
    it('should return -1 when the value is not present');
  });
});
