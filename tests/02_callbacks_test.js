const assert = require('assert');

describe('2. Callbacks', function() {
  describe('#done()', function() {
    it('only one should be right', function(done) {
      setImmediate(done);
    });
  });
  describe('#done2()', function() {
    it('double done is an error', function(done) {
      setImmediate(done);
      setImmediate(done);
    });
  });
});
