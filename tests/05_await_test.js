var chai = require('chai')
var expect = chai.expect

describe('5. Async Programming', function() {
  describe('#async/await', function() {
    it("Using a Promise with async/await that resolves successfully", async function() {
        var testPromise = new Promise(function(resolve, reject) {
            setTimeout(function() {
                resolve("Hello World!");
            }, 200);
        });

        var result = await testPromise;

        expect(result).to.equal("Hello World!");
    });
  });
});
