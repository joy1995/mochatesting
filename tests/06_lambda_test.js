const assert = require('assert');


describe('6. Lambda', function() {

  describe('#without lambda', function() {
    it('can access the Mocha context', function() {
      // should set the timeout of this test to 1000 ms; instead will fail
      this.timeout(1000);
      assert.ok(true);
    });
  });

  describe('with lambda', () => {
    it('cannot access the Mocha context', () => {
      // should set the timeout of this test to 1000 ms; instead will fail
      this.timeout(1000);
      assert.ok(true);
    });
  });
});
